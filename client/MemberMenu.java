package client;

public class MemberMenu {
	public static void mainDispaly() {
		System.out.println("=============");
		System.out.println("1. 회원정보확인");
		System.out.println("2. 회원정보변경");
		System.out.println("3. 회원탈퇴");
		System.out.println("0. 돌아가기");
		int num = Client.intInput();

		switch (num) {
		case 1: // 회원정보확인
			break;
		case 2: // 회원정보변경
			memberInfoChange();
			break;
		case 3: // 회원탈퇴
			break;
		case 0: // 돌아가기
		default:
			System.out.println("잘못된 입력입니다.");
			return;
		}
		Client.receive();
	}

	public static void memberInfoChange() {
		System.out.println("이름 : ");
		Client.stringInput();
		System.out.println("주소 : ");
		Client.stringInput();
		System.out.println("전화번호 : ");
		Client.stringInput();
	}
}
