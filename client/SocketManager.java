package client;

import java.io.*;
import java.net.*;

public class SocketManager {
	private Socket clientSocket;
	private OutputStream outputData;
	private InputStream inputData;
	private PrintWriter prtWriter;
	private BufferedReader bufReader;

	public SocketManager() {
		try {
			clientSocket = new Socket("localhost", 9999);
			outputData = clientSocket.getOutputStream();
			inputData = clientSocket.getInputStream();
		} catch (IOException e) {
			System.out.println("Socket error");
			e.printStackTrace();
		}
		prtWriter = new PrintWriter(new BufferedWriter(new OutputStreamWriter(outputData)));
		bufReader = new BufferedReader(new InputStreamReader(inputData));
	}

	public void write(String sendData) {
		prtWriter.println(sendData);
		prtWriter.flush();
	}

	public void read() {
		String receiveData = null;
		Boolean exitFalg = false;
		try {
			while (false == exitFalg) {
				receiveData = bufReader.readLine();
				if ((receiveData == null) || receiveData.equals("\u0004")) {
					exitFalg = true;
					break;
				}
				System.out.println(receiveData);
			}
		} catch (IOException e) {
			System.out.println("buffer read error");
			e.printStackTrace();
		}
	}

	public void close() {
		try {
			clientSocket.close();
		} catch (IOException e) {
			System.out.println("socket close error");
			e.printStackTrace();
		}
	}
}
