package client;

public class BookMenu {
	public static void mainDispaly() {
		System.out.println("=============");
		System.out.println("1. 도서목록");
		System.out.println("2. 도서대여");
		System.out.println("3. 도서예약");
		System.out.println("4. 도서반납");
		System.out.println("0. 돌아가기");

		int num = Client.intInput();

		switch (num) {
		case 1: // 도서목록
			break;
		case 2: // 도서대여
			bookRent();
			break;
		case 3: // 도서예약
			bookReserve();
			break;
		case 4: // 도서반납
			bookReturn();
			break;
		case 0: // 돌아가기
		default:
			System.out.println("잘못된 입력입니다.");
			return;
		}
		Client.receive();
	}

	public static void bookRent() {
		System.out.println("대여할 책 번호 입력 : ");
		Client.intInput();
	}

	public static void bookReserve() {
		System.out.println("예약할 책 번호 입력 : ");
		Client.intInput();
	}

	public static void bookReturn() {
		System.out.println("반납할 책 번호 입력 : ");
		Client.intInput();
	}
}
