package client;

import java.util.*;

public class Client {
	static SocketManager socketManager;
	static Scanner scan;
	public static void main(String[] args) {
		socketManager = new SocketManager();
		scan = new Scanner(System.in);
		mainDisplay();
	}

	public static void mainDisplay() {
		Boolean exitFlag = false;
		
		while (false == exitFlag) {
			System.out.println("===================");
			System.out.println("1. 로그인");
			System.out.println("2. 회원가입");
			System.out.println("0. 종료");

			int num = Client.intInput();
			
			switch (num) {
			case 1:
				login();
				break;
			case 2:
				signin();
				break;
			case 0:
				exitFlag = true;
				System.out.println("프로그램을 종료합니다.");
				break;
			default:
				System.out.println("잘못된 입력입니다.");
				break;
			}
		}
		scan.close();
	}
	
	public static int intInput() {
		String input;
		input = scan.nextLine();
		socketManager.write(input);
		return Integer.parseInt(input);
	}
	
	public static int intInput(int num) {
		socketManager.write(Integer.toString(num));
		return num;
	}
	
	public static String stringInput() {
		String input;
		input = scan.nextLine();
		socketManager.write(input);
		return input;
	}
	
	public static void receive() {
		socketManager.read();
	}

	public static void login() {
		System.out.println("ID 입력 : ");
		Client.intInput();
		Boolean exitFlag = false;
		while (false == exitFlag) {
			System.out.println("=============");
			System.out.println("1. 도서관련");
			System.out.println("2. 회원관련");
			System.out.println("3. 로그아웃");

			int num = Client.intInput();
			
			switch (num) {
			case 1:
				BookMenu.mainDispaly();
				break;
			case 2:
				MemberMenu.mainDispaly();
				break;
			case 3:
				exitFlag = true;
				break;
			default:
				break;
			}
		}
		
	}
	
	public static void signin() {
		System.out.println("이름 : ");
		Client.stringInput();
		System.out.println("주소 : ");
		Client.stringInput();
		System.out.println("전화번호 : ");
		Client.stringInput();
		
		Client.receive();
		System.out.println("가입OK");
	}
}
