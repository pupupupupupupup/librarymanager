package server;

public class ProcessThread extends Thread {
	private User user;

	public ProcessThread(User user) {
		this.user = user;
	}

	public void run() {
		mainDisplay();
	}

	public void mainDisplay() {
		Boolean exitFlag = false;
		while (false == exitFlag) {
			int num = Integer.parseInt(user.receive());

			switch (num) {
			case 1:
				user.login(user.receive());
				break;
			case 2:
				user.signin();
				break;
			case 0:
				exitFlag = true;
				break;
			default:
				break;
			}
		}
	}
}
