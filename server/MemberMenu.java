package server;

public class MemberMenu {
	public static void way(int num, User user) {
		String sql = null;
		String name, addr, phone;
		switch (num) {
		case 1: // 회원정보확인
			sql = "call showmeminfo(" + user.getID() + ");";
			break;
		case 2: // 회원정보변경
			name = user.receive();
			addr = user.receive();
			phone = user.receive();
			sql = "";
			break;
		case 3: // 회원탈퇴
			sql = "";
			break;
		case 0: // 돌아가기
		default:
			return;
		}
		user.procedure(sql);
	}
}
