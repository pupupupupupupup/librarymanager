package server;

import java.io.*;
import java.net.*;

public class SocketManager {
	private Socket clientSocket;
	private OutputStream outputData;
	private InputStream inputData;
	private PrintWriter prtWriter;
	private BufferedReader bufReader;

	public SocketManager(ServerSocket serverSocket) {
		try {
			clientSocket = serverSocket.accept();
			outputData = clientSocket.getOutputStream();
			inputData = clientSocket.getInputStream();
		} catch (IOException e) {
			System.out.println("ClientSocket error");
			e.printStackTrace();
		}
		prtWriter = new PrintWriter(new BufferedWriter(new OutputStreamWriter(outputData)));
		bufReader = new BufferedReader(new InputStreamReader(inputData));
	}

	public void write(String sendData) {
		prtWriter.println(sendData);
		prtWriter.flush();
		prtWriter.print("\u0004");
		prtWriter.flush();
	}

	public String read() {
		String receiveData = null;
		try {
			receiveData = bufReader.readLine();
		} catch (IOException e) {
			System.out.println("buffer read error");
			e.printStackTrace();
		}
		return receiveData;
	}

	public void close() {
		try {
			clientSocket.close();
		} catch (IOException e) {
			System.out.println("socket close error");
			e.printStackTrace();
		}
	}

}
