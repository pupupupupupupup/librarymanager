package server;

import java.io.*;
import java.net.*;
import java.util.*;

public class Server {
	static DBManager DBManager = new DBManager();
	public static void main(String[] args) {
		DBManager.connect();
		ServerSocket serverSocket = null;
		try {
			serverSocket = new ServerSocket(9999);
		} catch (IOException e) {
			System.out.println("server socket error");
			e.printStackTrace();
		}
		ArrayList<Thread> threadArray = new ArrayList<Thread>();
		while (true) {
			SocketManager socketManager = new SocketManager(serverSocket);
			ProcessThread thread = new ProcessThread(new User(socketManager));
			thread.start();
			threadArray.add(thread);
		}
	}
}

