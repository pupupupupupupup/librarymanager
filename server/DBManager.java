package server;

import java.sql.*;

public class DBManager {
	private String driver;
	private String userName;
	private String password;
	private String DBURL;
	private Connection connection;
	private Statement st;

	public DBManager(String driver) {
		driver = "com.mysql.cj.jdbc.Driver";
		userName = "root";
		password = "rlawogml123";
		DBURL = "jdbc:mysql://localhost:3306/test_01?serverTimezone=UTC&characterEncoding=UTF-8&useUnicode=true";
	}

	public void connect() {
		System.out.println("DB 연결중...");
		try {
			Class.forName(driver);
		} catch (ClassNotFoundException e1) {
			System.out.println("driver error");
			e1.printStackTrace();
		}
		try {
			connection = DriverManager.getConnection(DBURL, userName, password);
			st = connection.createStatement();
		} catch (SQLException e) {
			System.out.println("connect error");
			e.printStackTrace();
		}
		System.out.println("DB 연결 OK");
	}

	public void close() {
		System.out.println("DB 연결해제중...");
		try {
			st.close();
			connection.close();
		} catch (SQLException e1) {
			System.out.println("connection close error");
			e1.printStackTrace();
		}
		System.out.println("DB 연결해제 OK");
	}

	public CallableStatement prepare(String query) {
		CallableStatement procedure = null;
		try {
			procedure =  connection.prepareCall(query);
		} catch (SQLException e) {
			System.out.println("procedure prepare error");
			e.printStackTrace();
		}
		return procedure;
	}
}
