package server;

import java.sql.*;

public class User {
	private int ID;
	private SocketManager socketManager;

	public User(SocketManager socketManager) {
		this.socketManager = socketManager;
	}

	public void login(String data) {
		this.ID = Integer.parseInt(data);
		System.out.println(ID + "번 회원 접속");

		Boolean exitFlag = false;
		while (false == exitFlag) {
			int num = Integer.parseInt(receive());
			
			switch (num) {
			case 1:
				BookMenu.way(Integer.parseInt(receive()), this);
				break;
			case 2:
				MemberMenu.way(Integer.parseInt(receive()), this);
				break;
			case 3:
				exitFlag = true;
				break;
			default:
				break;
			}
		}
	}
	
	public void signin() {
		String name = receive();
		String addr = receive();
		String phone = receive();
		String sql = "CALL SIGNUP('" + name + "','" + addr + "','" + phone + "');"; 
		procedure(sql);
		System.out.println("가입종료");
	}

	public void send(String sendData) {
		socketManager.write(sendData);
	}

	public String receive() {
		return socketManager.read();
	}

	public int getID() {
		return this.ID;
	}

	public void procedure(String query) {
		CallableStatement procedure = Server.DBManager.prepare(query);
		boolean isExistResult = false;
		try {
			isExistResult = procedure.execute();
		} catch (SQLException e) {
			System.out.println("procedure execute error");
			e.printStackTrace();
		}
		ResultSet rs = null;
		ResultSetMetaData rsmd = null;
		int columnCount = 0;

		while (isExistResult) {
			try {
				rs = procedure.getResultSet();
				rsmd = rs.getMetaData();
				columnCount = rsmd.getColumnCount();
			} catch (SQLException e) {
				System.out.println("getResult error");
				e.printStackTrace();
			}

			String sqlRecipeProcess = "";
			try {
				while (rs.next()) {
					for (int i = 1; i <= columnCount; ++i)
						sqlRecipeProcess += rs.getString(i) + " ";
					send(sqlRecipeProcess);
					sqlRecipeProcess = "";
				}
			} catch (SQLException e) {
				System.out.println("result set next error");
				e.printStackTrace();
			}
			try {
				rs.close();
			} catch (SQLException e) {
				System.out.println("result set close error");
				e.printStackTrace();
			}
			try {
				isExistResult = procedure.getMoreResults();
			} catch (SQLException e) {
				System.out.println("getMoreResult error");
				e.printStackTrace();
			}
		}
	}
}
